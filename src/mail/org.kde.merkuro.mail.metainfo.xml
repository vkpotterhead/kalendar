<?xml version="1.0" encoding="utf-8"?>
<component type="desktop-application">
  <!--
  SPDX-FileCopyrightText: 2021 Carl Schwan <carl@carlschwan.eu>
  SPDX-FileCopyrightText: 2021 Claudio Cambra <claudio.cambra@gmail.com>
  SPDX-License-Identifier: CC0-1.0
  -->
  <id>org.kde.merkuro.mail</id>
  <name>Merkuro Mail</name>
  <name xml:lang="ca">Merkuro Mail</name>
  <name xml:lang="ca-valencia">Correu de Merkuro</name>
  <name xml:lang="es">Correo de Merkuro</name>
  <name xml:lang="fi">Merkuro Mail</name>
  <name xml:lang="fr">Merkuro Mail</name>
  <name xml:lang="ia">Merkuro Mail</name>
  <name xml:lang="it">Merkuro Posta</name>
  <name xml:lang="ka">Merkuro Mail</name>
  <name xml:lang="nl">Merkuro-e-mail</name>
  <name xml:lang="pt">E-Mail do Merkuro</name>
  <name xml:lang="sl">Pošta Merkuro</name>
  <name xml:lang="tr">Erdenay Posta</name>
  <name xml:lang="uk">Пошта Merkuro</name>
  <name xml:lang="x-test">xxMerkuro Mailxx</name>
  <summary>Read your emails with speed and ease</summary>
  <summary xml:lang="ca">Llegiu els correus amb rapidesa i facilitat</summary>
  <summary xml:lang="ca-valencia">Llegiu els correus amb rapidesa i facilitat</summary>
  <summary xml:lang="de">Lesen Sie schnell und einfach Ihre E-Mails</summary>
  <summary xml:lang="es">Lea el correo con rapidez y facilidad</summary>
  <summary xml:lang="fi">Lue sähköpostisi nopeasti ja helposti</summary>
  <summary xml:lang="fr">Gérer efficacement et facilement vos courriels </summary>
  <summary xml:lang="ia">Lege tu messages de e-posta con rapiditate e facilitate</summary>
  <summary xml:lang="it">Leggi i tuoi messaggi di posta con velocità e facilità</summary>
  <summary xml:lang="ka">წაიკითხეთ თქვენი ელფოსტა სწრაფად და იოლად</summary>
  <summary xml:lang="nl">Uw e-mails lezen met snelheid en gemak</summary>
  <summary xml:lang="pt">Leia as suas mensagens com rapidez e facilidade</summary>
  <summary xml:lang="sl">Berite svoji pošto, hitro in enostavno</summary>
  <summary xml:lang="tr">E-postalarınızı hızlıca ve kolayca yönetin</summary>
  <summary xml:lang="uk">Швидко і ефективно читайте вашу електронну пошту</summary>
  <summary xml:lang="x-test">xxRead your emails with speed and easexx</summary>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <developer_name>KDE Community, Clau Cambra and Carl Schwan</developer_name>
  <developer_name xml:lang="ca">Comunitat KDE, Clau Cambra i Carl Schwan</developer_name>
  <developer_name xml:lang="ca-valencia">Comunitat KDE, Clau Cambra i Carl Schwan</developer_name>
  <developer_name xml:lang="de">KDE-Gemeinschaft, Clau Cambra und Carl Schwan</developer_name>
  <developer_name xml:lang="es">La Comunidad KDE, Clau Cambra y Carl Schwan</developer_name>
  <developer_name xml:lang="fi">KDE-yhteisö, Clau Cambra ja Carl Schwan</developer_name>
  <developer_name xml:lang="fr">Communauté de KDE, Clau Cambra et Carl Schwan</developer_name>
  <developer_name xml:lang="ia">Communitate de KDE, Clau Cambra e Carl Schwan</developer_name>
  <developer_name xml:lang="it">Comunità KDE, Clau Cambra e Carl Schwan</developer_name>
  <developer_name xml:lang="ka">KDE -ის საზოგადოება, Clau Cambra და Carl Schwan</developer_name>
  <developer_name xml:lang="nl">KDE-gemeenschap, Clau Cambra en Carl Schwan</developer_name>
  <developer_name xml:lang="pt">Comunidade do KDE, Clau Cambra e Carl Schwan</developer_name>
  <developer_name xml:lang="sl">Skupnost KDE, Clau Cambra in Carl Schwan</developer_name>
  <developer_name xml:lang="tr">KDE Topluluğu, Clau Cambra ve Carl Schwan</developer_name>
  <developer_name xml:lang="uk">Спільнота KDE, Clau Cambra і Carl Schwan</developer_name>
  <developer_name xml:lang="x-test">xxKDE Community, Clau Cambra and Carl Schwanxx</developer_name>
  <description>
    <p>Merkuro Mail is a free and open source email client. Merkuro Mail is part of the Merkuro groupware suite and is currently in beta. For now, it only allows you to read your emails.</p>
  </description>
  <requires>
    <display_length compare="ge">360</display_length>
  </requires>
  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>
  <provides>
    <binary>merkuro-mail</binary>
  </provides>
  <url type="homepage">https://apps.kde.org/merkuro.mail/</url>
  <url type="bugtracker">https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product=merkuro</url>
  <content_rating type="oars-1.1"/>
  <releases>
    <release version="23.04.3" date="2023-07-06"/>
    <release version="23.04.2" date="2023-06-08"/>
    <release version="23.04.1" date="2023-05-11"/>
    <release version="23.04.0" date="2023-04-20"/>
    <release version="22.12.3" date="2023-03-02"/>
    <release version="22.12.2" date="2023-02-02"/>
    <release version="22.12.1" date="2023-01-05"/>
    <release version="22.12.0" date="2022-12-08"/>
    <release version="22.08.3" date="2022-11-03"/>
    <release version="22.08.2" date="2022-10-13"/>
    <release version="22.08.1" date="2022-09-08"/>
  </releases>
  <screenshots>
    <screenshot type="default">
      <image>https://cdn.kde.org/screenshots/merkuro/mail.png</image>
      <caption>Mail View</caption>
    </screenshot>
  </screenshots>
  <custom>
    <value key="KDE::matrix">#merkuro:kde.org</value>
    <value key="KDE::mastodon">https://kde.social/@merkuro</value>
  </custom>
  <launchable type="desktop-id">org.kde.merkuro.mail.desktop</launchable>
</component>
